#region Legal and Info.
//////////////////////////////////////////////////////////////////////////////////////////////////////
/// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL 
/// WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
/// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL 
/// THE AUTHOR BE LIABLE FOR ANY SPECIAL,
/// DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER 
/// RESULTING FROM LOSS OF USE, DATA OR PROFITS,
/// WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, 
/// ARISING OUT OF OR IN CONNECTION WITH THE USE
/// OR PERFORMANCE OF THIS SOFTWARE.
///
/// Debuffs is Licensed under a Creative Commons Attribution-NonCommercial 3.0 Unported License.
/// For more info:
/// http://creativecommons.org/licenses/by-nc/3.0/deed.en_US
/// 
/// The source and latest version of this plugin can be found at simius bitbucket repo.
/// https://bitbucket.org/simius/decalplugin-debuffs
//////////////////////////////////////////////////////////////////////////////////////////////////////
#endregion
using Decal.Adapter;
using System.Collections.Generic;
namespace Debuffs
{
	/// <summary>
	/// PluginCore.
	/// </summary>
	[FriendlyName("Debuffs")]
	public partial class PluginCore : PluginBase
	{
		/// <summary>
		/// Tick in milliseconds.
		/// </summary>
		const int TICKTIME = 1000;

		private List<Debuffable> debuffable;
		private DebuffHud debuffHud;
		private System.Windows.Forms.Timer mainTimer;
		
		/// <summary>
		/// Effects table.
		/// Any new effect to track should be added here.
		/// </summary>
		private static Dictionary<int, Effect> effects = new Dictionary<int, Effect>()
		{
			{ 38, new Effect(2178, "Fester", eSpellSchool.Life, eSpellType.Debuff, 60 * 4) },
			{ 44, new Effect(2170, "Fire Vulnerability", eSpellSchool.Life, eSpellType.Debuff, 60 * 4) },
			{ 46, new Effect(2174, "Pierc Vulnerability", eSpellSchool.Life, eSpellType.Debuff, 60 * 4) },
			{ 48, new Effect(2164, "Slash Vlunerability", eSpellSchool.Life, eSpellType.Debuff, 60 * 4) },
			{ 50, new Effect(2162, "Acid Vulnerability", eSpellSchool.Life, eSpellType.Debuff, 60 * 4) },
			{ 52, new Effect(2168, "Cold Vulnerability", eSpellSchool.Life, eSpellType.Debuff, 60 * 4) },
			{ 54, new Effect(2172, "Lightning Vulnerability", eSpellSchool.Life, eSpellType.Debuff, 60 * 4) },
			 //56 Can be both imp and bludge vuln.
			{ 56, new Effect(2074, "Imperil",eSpellSchool.Life, eSpellType.Debuff, 60 * 4) },
			//set bludge vuln key to 999 so its easy to fetch. Real key: 56 (used by imp).
			{ 999, new Effect(2166, "Bludge Vulnerability", eSpellSchool.Life, eSpellType.Debuff, 60 * 4) },
			//Change Void ones to correct lifespan.
			{ 167, new Effect(5337, "Destructive Curse", eSpellSchool.Void, eSpellType.Debuff, 30) },
			{ 168, new Effect(5377, "Festering Curse", eSpellSchool.Void, eSpellType.Debuff, 30) },
			{ 169, new Effect(5385, "Weakening Curse", eSpellSchool.Void, eSpellType.Debuff,15) }, 
			//Dispells need no spell ID nor lifespan, they are never applied as a debuff, only removes others and disapear.
			{ 145, new Effect(0, "Dispell", eSpellSchool.Life, eSpellType.Dispell, 0) },
			{ 146, new Effect(0, "Dispell", eSpellSchool.Creature, eSpellType.Dispell, 0) }
		};

		/// <summary>
		/// Fetches an effect from the effect table.
		/// </summary>
		/// <param name="id">Effect id.</param>
		/// <returns>Effect.</returns>
		public static Effect GetEffect(int id)
		{
			return (effects.ContainsKey(id) ? effects[id] : null);
		}
		
		protected override void Startup()
		{
			debuffHud = new DebuffHud();
			debuffable = new List<Debuffable>();
			mainTimer = new System.Windows.Forms.Timer();
			//Set up events.
			Core.WorldFilter.CreateObject += new System.EventHandler<Decal.Adapter.Wrappers.CreateObjectEventArgs>(WorldFilterCreateObject);
			Core.WorldFilter.ReleaseObject += new System.EventHandler<Decal.Adapter.Wrappers.ReleaseObjectEventArgs>(WorldFilterReleaseObject);
			ServerDispatch += new System.EventHandler<NetworkMessageEventArgs>(PluginCoreServerDispatch);
			Core.ItemSelected += new System.EventHandler<ItemSelectedEventArgs>(CoreItemSelected);
			mainTimer.Tick += new System.EventHandler(MainTimerTick);
			Core.CommandLineText += new System.EventHandler<ChatParserInterceptEventArgs>(CoreCommandLineText);
		
			mainTimer.Interval = TICKTIME;
			mainTimer.Start();

			Core.Actions.AddChatText("Debuffs initialized. To turn hud off and on type /debuffs hide or /debuffs show", 5, 1);
		}

		protected override void Shutdown()
		{
			mainTimer.Stop();
			mainTimer.Dispose();
			debuffable.Clear();
		}

		#region Event callbacks

		void CoreCommandLineText(object sender, ChatParserInterceptEventArgs e)
		{
			if (e.Text[0] == '/')
			{
				string command = e.Text.Substring(1).ToUpper();
				if (command.StartsWith("DEBUFFS "))
				{
					command = command.Replace("DEBUFFS ", "");
					switch (command)
					{
						case "HIDE":
							debuffHud.SetState(false);
							break;
						case "SHOW":
							debuffHud.SetState(true);
							break;
						default:
							Core.Actions.AddChatText("Error, Debuffs-command does not exist.", 5, 1);
							break;
					}
				}
			}
		}

		void CoreItemSelected(object sender, ItemSelectedEventArgs e)
		{
			if (Core.WorldFilter[e.ItemGuid].ObjectClass == Decal.Adapter.Wrappers.ObjectClass.Player || Core.WorldFilter[e.ItemGuid].ObjectClass == Decal.Adapter.Wrappers.ObjectClass.Monster)
			{
				//Find debuffable.
				foreach (Debuffable d in debuffable)
				{
					if (d.GUID == e.ItemGuid)
					{
						Debug.Print("Selected a debuffable.");
						debuffHud.SetTarget(d);
						return;
					}
				}
				Debug.Print("Selected a monster or player, but it was not counted as a debuffable, cause it was not in the list.");
			}
			debuffHud.ClearTarget();
		}

		private void MainTimerTick(object sender, System.EventArgs e)
		{
			for (int i = 0; i < debuffable.Count; i++)
				debuffable[i].Update(TICKTIME / 1000.0f);
			debuffHud.UpdateHud();
		}

		private void WorldFilterReleaseObject(object sender, Decal.Adapter.Wrappers.ReleaseObjectEventArgs e)
		{
			//Only do stuff if it is a player or monster.
			if (e.Released.ObjectClass != Decal.Adapter.Wrappers.ObjectClass.Player && e.Released.ObjectClass != Decal.Adapter.Wrappers.ObjectClass.Monster)
				return;
			int index = -1;
			for (int i = 0; i < debuffable.Count; i++)
			{
				if (debuffable[i].GUID == e.Released.Id)
				{
					index = i;
					break;
				}
			}
			if (index != -1)
			{
				if (e.Released.Id == debuffHud.CurrentTarget)
					debuffHud.ClearTarget();
				debuffable.RemoveAt(index);
				Debug.Print("Removing object from 'debuffables'. Id: " + e.Released.Id + " Name: " + e.Released.Name);
			}
			else
				Debug.Print("An object was released, but it was not in the list of debuffables. Name: " + e.Released.Name);
		}

		private void WorldFilterCreateObject(object sender, Decal.Adapter.Wrappers.CreateObjectEventArgs e)
		{
			//Only do stuff if it is a player or monster.
			if (e.New.ObjectClass != Decal.Adapter.Wrappers.ObjectClass.Player && e.New.ObjectClass != Decal.Adapter.Wrappers.ObjectClass.Monster)
				return;
			int index = -1;
			for (int i = 0; i < debuffable.Count; i++)
			{
				if (debuffable[i].GUID == e.New.Id)
				{
					index = i;
					break;
				}
			}
			if (index == -1)
			{
				Debug.Print("Adding object to 'debuffables'. Id: " + e.New.Id + " Name: " + e.New.Name);
				debuffable.Add(new Debuffable(e.New.Id, e.New.Name, e.New.Icon));
			}
			else
				Debug.Print("An object was created, but already in the list. Name: " + e.New.Name);
		}

		private void PluginCoreServerDispatch(object sender, NetworkMessageEventArgs e)
		{
			if (e.Message.Type == 0xF755)
				TryApplyEffect(e.Message);
		}

		private void TryApplyEffect(Message message)
		{
			int objectId = message.Value<int>("object");
			int effectId = message.Value<int>("effect");
			Effect e = GetEffect(effectId);
			if (e == null)
				return;
			//If dispell, dispell.
			if (e.Type == eSpellType.Dispell)
			{
				//Its not sure that this is actualy a dispell that is high enough level etc, but to be safe, the debuffs of given dispell school will be removed.
				foreach (Debuffable d in debuffable)
				{
					if (d.GUID == objectId)
					{
						Debug.Print("Removing all effects from school: " + e.School.ToString());
						d.RemoveDebuffs(e.School);
						return;
					}
				}
				return;
			}
			//Apply effect.
			for (int j = 0; j < debuffable.Count; j++)
			{
				if (debuffable[j].GUID == objectId)
				{
					Debug.Print("Applying effect with id : " + effectId + " to object with id: " + objectId + " (name: " + debuffable[j].Name + ")");
					debuffable[j].AddDebuff(e);
					return;
				}
			}
		}

		#endregion

	}
}
