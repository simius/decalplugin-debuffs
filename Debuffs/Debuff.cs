﻿#region Legal and Info.
//////////////////////////////////////////////////////////////////////////////////////////////////////
/// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL 
/// WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
/// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL 
/// THE AUTHOR BE LIABLE FOR ANY SPECIAL,
/// DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER 
/// RESULTING FROM LOSS OF USE, DATA OR PROFITS,
/// WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, 
/// ARISING OUT OF OR IN CONNECTION WITH THE USE
/// OR PERFORMANCE OF THIS SOFTWARE.
///
/// Debuffs is Licensed under a Creative Commons Attribution-NonCommercial 3.0 Unported License.
/// For more info:
/// http://creativecommons.org/licenses/by-nc/3.0/deed.en_US
/// 
/// The source and latest version of this plugin can be found at simius bitbucket repo.
/// https://bitbucket.org/simius/decalplugin-debuffs
//////////////////////////////////////////////////////////////////////////////////////////////////////
#endregion
namespace Debuffs
{
	/// <summary>
	/// Spell schools as enum.
	/// </summary>
	public enum eSpellSchool
	{
		Life,
		Creature,
		Void,
		Item,
		All
	}

	/// <summary>
	/// Type of spell as enum.
	/// </summary>
	public enum eSpellType
	{
		Debuff,
		Buff,
		Dispell
	}

	/// <summary>
	/// Effect.
	/// </summary>
	public class Effect
	{
		/// <summary>
		/// Id of the spell.
		/// </summary>
		public int SpellId { get; private set; }
		/// <summary>
		/// Name of the effect.
		/// </summary>
		public string Name { get; private set; }
		/// <summary>
		/// Spell school.
		/// </summary>
		public eSpellSchool School { get; private set; }
		/// <summary>
		/// TYpe of spell.
		/// </summary>
		public eSpellType Type { get; private set; }
		/// <summary>
		/// Lifespan in seconds.
		/// </summary>
		public float Lifespan { get; private set; }

		/// <summary>
		/// Effect.
		/// </summary>
		/// <param name="effectId">Id of the effect.</param>
		/// <param name="spellId">Spell ID (Any level, used for icons).</param>
		/// <param name="name">Name of the effect.</param>
		/// <param name="school">School of magic (Creature, Life, Item, War, Void).</param>
		/// <param name="spellType">Type of spell (Debuff, Dispell etc).</param>
		/// <param name="lifespan">Lifespan in seconds.</param>
		public Effect(int spellId, string name, eSpellSchool school, eSpellType spellType, float lifespan)
		{
			this.SpellId = spellId;
			this.Name = name;
			this.School = school;
			this.Type = spellType;
			this.Lifespan = lifespan;
		}
	}

	/// <summary>
	/// Debuff.
	/// </summary>
	public class Debuff
	{
		/// <summary>
		/// Type of debuff.
		/// </summary>
		public Effect Effect { get { return this.effect; } }
		/// <summary>
		/// Lifespan of debuff.
		/// </summary>
		public float LifeSpan { set { this.lifespan = value; } get { return lifespan; } }

		private float lifespan;
		private Effect effect;

		/// <summary>
		/// Wether this debuff should be removed next tick or not.
		/// </summary>
		public bool Remove { get; set; }

		/// <summary>
		/// Creates a debuff object.
		/// </summary>
		/// <param name="effect">Debuff as Effect.</param>
		public Debuff(Effect effect)
		{
			this.lifespan = effect.Lifespan;
			this.effect = effect;
			this.Remove = false;
		}

		/// <summary>
		/// Updates the timer on the Debuff.
		/// </summary>
		/// <param name="dt">Delta Time in seconds.</param>
		public void Update(float dt)
		{
			lifespan -= dt;
			if (lifespan <= 0)
				this.Remove = true;
		}
	}
}
