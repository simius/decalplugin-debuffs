﻿#region Legal and Info.
//////////////////////////////////////////////////////////////////////////////////////////////////////
/// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL 
/// WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
/// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL 
/// THE AUTHOR BE LIABLE FOR ANY SPECIAL,
/// DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER 
/// RESULTING FROM LOSS OF USE, DATA OR PROFITS,
/// WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, 
/// ARISING OUT OF OR IN CONNECTION WITH THE USE
/// OR PERFORMANCE OF THIS SOFTWARE.
///
/// Debuffs is Licensed under a Creative Commons Attribution-NonCommercial 3.0 Unported License.
/// For more info:
/// http://creativecommons.org/licenses/by-nc/3.0/deed.en_US
/// 
/// The source and latest version of this plugin can be found at simius bitbucket repo.
/// https://bitbucket.org/simius/decalplugin-debuffs
//////////////////////////////////////////////////////////////////////////////////////////////////////
#endregion
using System.Collections.Generic;
namespace Debuffs
{
	/// <summary>
	/// Debuffable.
	/// Can either be a player or a monster.
	/// </summary>
	public class Debuffable
	{
		/// <summary>
		/// GUID of the object.
		/// </summary>
		public int GUID { get; private set; }
		/// <summary>
		/// Name of the debuffable object.
		/// </summary>
		public string Name { get; private set; }
		/// <summary>
		/// Current debuffs on the object.
		/// </summary>
		public List<Debuff> Debuffs { get { return debuffs;} }
		/// <summary>
		/// Icon id.
		/// </summary>
		public int Icon { get; private set; }

		private List<Debuff> debuffs;

		/// <summary>
		/// Debuffable object.
		/// This can be either a player or a monster.
		/// </summary>
		/// <param name="guid">GUID of the object.</param>
		/// <param name="name">Name of the object.</param>
		public Debuffable(int guid, string name, int icon)
		{
			this.Name = name;
			this.GUID = guid;
			this.debuffs = new List<Debuff>();
			this.Icon = icon;
		}

		/// <summary>
		/// Updates all debuff timers on the object.
		/// </summary>
		/// <param name="dt">Delta Time in seconds.</param>
		public void Update(float dt)
		{
			foreach (Debuff d in debuffs)
				d.Update(dt);
			for (int i = debuffs.Count; i-- > 0; )
			{
				if (debuffs[i].Remove)
					debuffs.RemoveAt(i);
			}
		}

		/// <summary>
		/// Adds a debuff to the object.
		/// </summary>
		/// <param name="type">Type of debuff.</param>
		/// <param name="lifespan">Lifespan of the debuff.</param>
		public void AddDebuff(Effect debuff)
		{
			//Temporary cheat fix for bludge vuln and imp.
			if (debuff.SpellId == 2074)
			{
				byte count = 0;
				//Check if imp and bludge vuln is on already.
				foreach (Debuff d in debuffs)
				{
					if (d.Effect.SpellId == 2074)
						count++;
				}
				Debug.Print("Added effect to object. Effect was Imp or Bludge vuln. Count: " + count);
				if(count == 1) //if one already set it to bludge vuln. (I know this is kinda ugly, but for now it will do).
					debuff = PluginCore.GetEffect(999); //Bludge vuln.
			}
			Debug.Print("Debuff spellName: " + debuff.Name);
			foreach (Debuff d in debuffs)
			{
				if (d.Effect.SpellId == debuff.SpellId) //Check with spellId, cause effect ID can occur multiple times.
				{
					d.LifeSpan = debuff.Lifespan;
					return;
				}
			}
			debuffs.Add(new Debuff(debuff));
		}

		/// <summary>
		/// Removes all debuffs from a given school from the object.
		/// This can be usefull in case of Eradicate spells etc.
		/// </summary>
		public void RemoveDebuffs(eSpellSchool school)
		{
			for (int i = debuffs.Count; i-- > 0; )
			{
				if (debuffs[i].Effect.School == school)
					debuffs.RemoveAt(i);
			}
		}
	}
}
