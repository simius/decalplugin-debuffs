﻿#region Legal and Info.
//////////////////////////////////////////////////////////////////////////////////////////////////////
/// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL 
/// WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
/// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL 
/// THE AUTHOR BE LIABLE FOR ANY SPECIAL,
/// DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER 
/// RESULTING FROM LOSS OF USE, DATA OR PROFITS,
/// WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, 
/// ARISING OUT OF OR IN CONNECTION WITH THE USE
/// OR PERFORMANCE OF THIS SOFTWARE.
///
/// Debuffs is Licensed under a Creative Commons Attribution-NonCommercial 3.0 Unported License.
/// For more info:
/// http://creativecommons.org/licenses/by-nc/3.0/deed.en_US
/// 
/// The source and latest version of this plugin can be found at simius bitbucket repo.
/// https://bitbucket.org/simius/decalplugin-debuffs
//////////////////////////////////////////////////////////////////////////////////////////////////////
#endregion
using VirindiViewService;
using VirindiViewService.Controls;
using System.Collections.Generic;
using Decal.Filters;
using Decal.Adapter;
namespace Debuffs
{
	/// <summary>
	/// DebuffHud.
	/// </summary>
	public class DebuffHud
	{
		//consts
		const string HEADERDEFAULT = "No target selected";
		const int ICONSCALE = 15;
		const int TEXTLENGTH = 35;

		/// <summary>
		/// GUID of current selected target.
		/// </summary>
		public int CurrentTarget { get { return currentTarget.GUID; } }

		private HudView hud;
		private List<HudPictureBox> images;
		private List<HudStaticText> timers;
		private HudStaticText name;
		private HudFixedLayout fixedLayout;
		private Debuffable currentTarget = null;

		/// <summary>
		/// DebuffHud
		/// </summary>
		public DebuffHud()
		{
			hud = new HudView("Debuffs", 350, 75, null);
			hud.ShowIcon = false;
			hud.ShowInBar = false;
			hud.Ghosted = true;
			hud.Visible = true;
			hud.UserGhostable = true;
			hud.LoadUserSettings();

			fixedLayout = new HudFixedLayout();
			hud.Controls.HeadControl = fixedLayout;

			name = new HudStaticText();
			name.Text = HEADERDEFAULT;
			fixedLayout.AddControl(name, new System.Drawing.Rectangle(10, 10, 350, 50));

			images = new List<HudPictureBox>();
			timers = new List<HudStaticText>();			
		}

		/// <summary>
		/// Set current target.
		/// </summary>
		public void SetTarget(Debuffable target)
		{
			currentTarget = target;
		}

		/// <summary>
		/// Clear the current target.
		/// </summary>
		public void ClearTarget()
		{
			currentTarget = null;
		}

		/// <summary>
		/// Updates the HUD.
		/// </summary>
		public void UpdateHud()
		{
			//Lazy way, might want to change this to something better later.
			foreach (HudStaticText t in timers)
				t.Dispose();
			foreach (HudPictureBox p in images)
				p.Dispose();
			timers.Clear();
			images.Clear();
			if (currentTarget == null)
			{
				name.Text = HEADERDEFAULT;
				return;
			}
			name.Text = currentTarget.Name;
			//Add icon if a mob.

			//new ACImage(0, true, ACImage.eACImageDrawOptions.DrawStretch);


			int y = ICONSCALE + 10;
			int x = 10;
			for (int i = 0; i < currentTarget.Debuffs.Count; i++)
			{
				Debuff debuff = currentTarget.Debuffs[i];
				x = 10 + ((ICONSCALE * i) + (TEXTLENGTH * i));
				if (i >= 6)
				{
					y = 45;
					x = 10 + ((ICONSCALE * (i-6)) + (TEXTLENGTH * (i - 6)));
				}
				HudPictureBox icon = new HudPictureBox();
				icon.Image = new ACImage(((FileService)CoreManager.Current.FileService).SpellTable.GetById(debuff.Effect.SpellId).IconId);
				images.Add(icon);				
				fixedLayout.AddControl(icon, new System.Drawing.Rectangle(x, y, ICONSCALE, ICONSCALE));
				HudStaticText txt = new HudStaticText();
				txt.Text = "(" + debuff.LifeSpan + ")";
				timers.Add(txt);
				fixedLayout.AddControl(txt, new System.Drawing.Rectangle(x + ICONSCALE, y, TEXTLENGTH, 20));
			}
		}

		/// <summary>
		/// Set hud vissible or hidden.
		/// </summary>
		public void SetState(bool visible)
		{
			hud.Visible = visible;
		}
	}
}
