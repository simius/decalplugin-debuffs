﻿#region Legal and Info.
//////////////////////////////////////////////////////////////////////////////////////////////////////
/// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL 
/// WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
/// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL 
/// THE AUTHOR BE LIABLE FOR ANY SPECIAL,
/// DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER 
/// RESULTING FROM LOSS OF USE, DATA OR PROFITS,
/// WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, 
/// ARISING OUT OF OR IN CONNECTION WITH THE USE
/// OR PERFORMANCE OF THIS SOFTWARE.
///
/// Debuffs is Licensed under a Creative Commons Attribution-NonCommercial 3.0 Unported License.
/// For more info:
/// http://creativecommons.org/licenses/by-nc/3.0/deed.en_US
/// 
/// The source and latest version of this plugin can be found at simius bitbucket repo.
/// https://bitbucket.org/simius/decalplugin-debuffs
//////////////////////////////////////////////////////////////////////////////////////////////////////
#endregion
namespace Debuffs
{
	/// <summary>
	/// Debug functions.
	/// If plugin is compiled in release, the functions will do nothing.
	/// </summary>
	public static class Debug
	{
		/// <summary>
		/// Print string to chat.
		/// </summary>
		/// <param name="msg">String to print.</param>
		public static void Print(string msg)
		{
#if DEBUG
			CoreManager.Current.Actions.AddChatText(msg, 5, 1);
#endif
		}
	}
}
